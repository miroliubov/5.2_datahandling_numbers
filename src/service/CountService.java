package src.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class CountService implements ICountService {

    @Override
    public void areaOfCircle(double radius) {
        System.out.println(new BigDecimal(radius).pow(2).multiply(new BigDecimal(Math.PI)).setScale(50, RoundingMode.HALF_UP));
    }

    @Override
    public void isSum(String number1, String number2, String number3) {
        if (number1.contains(",") || number2.contains(",") || number3.contains(",")) {
            number1 = number1.replace(',', '.');
            number2 = number2.replace(',', '.');
            number3 = number3.replace(',', '.');
        }
        BigDecimal bd1 = new BigDecimal(number1);
        BigDecimal bd2 = new BigDecimal(number2);
        BigDecimal bd3 = new BigDecimal(number3);
        if (bd1.add(bd2).compareTo(bd3) == 0) {
            System.out.println("Третье число равно сумме первых двух");
        }
        else System.out.println("Третье число НЕ равно сумме первых двух");
    }

    @Override
    public void findMinMax(int num1, int num2, int num3) {
        System.out.println("Максимальное число: " + Math.max(num1, Math.max(num2, num3)));
        System.out.println("Минимальное число: " + Math.min(num1, Math.min(num2, num3)));
    }


}
