package src.service;

public interface ICountService {

    void areaOfCircle(double radius);
    void isSum(String number1, String number2, String number3);
    void findMinMax(int num1, int num2, int num3);
}
