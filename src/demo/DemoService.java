package src.demo;

import src.service.CountService;
import src.service.ICountService;

public class DemoService implements IDemoService {

    @Override
    public void execute() {
        ICountService countService = new CountService();
        System.out.println("Задача 2.1");
        countService.areaOfCircle(4.320569874523);
        System.out.println("Задача 2.2");
        countService.isSum("2.5", "3.2", "5.7");
        countService.isSum("2.5", "3,2", "5,6");
        System.out.println("Задача 2.3");
        countService.findMinMax(123, 250, 17);
    }
}
